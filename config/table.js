const database = require('./database');

const dynogels = database.dynogels;
const ddb = database.ddb; //this is the dynamodb method obtained from aws-sdk

// const tableName = require('./config').TABLE_NAME;

const Joi = require('joi');
// let tableCreated = false;
// // Creating a table is an async operation
function createTables(tableName,callback) {
    const tableSettings = {};
    tableSettings[tableName] = { //default settings for the table
        readCapacity: 5,
        writeCapacity: 1
    }
    dynogels.createTables(tableSettings, (err) => {
    if (err) {
        console.log('Error creating table: ', err);
    } else {
        console.log('Table has been created');
        callback(true);
    }
});
};



//Check if the table exists
function checkTableState(tablename, statusCallback) {
    statusCallback = statusCallback || console.log;
    // let status = false;
    ddb.describeTable({ TableName: tablename }, (err, response) => {
        if (err) {
            console.log('err:', 'Table not found');
            statusCallback(false);
        }
        else {
            console.log(`${tablename} is the active table`);
            statusCallback(true)
        }
    });
};

function checkAndCreateTable(tableName, callback) {
    callback = callback || console.log;
    checkTableState(tableName, exists =>{
        if(!exists){
            createTables(tableName,done => {
               if (done) {
                   console.log(`Created the table: ${tableName}`);
                  callback();
               }
           });
       } else {
           console.log(`Table:'${tableName}' is active`);
           callback();
       }
    })
};

module.exports = {
    checkTableState,
    createTables,
    checkAndCreateTable
}